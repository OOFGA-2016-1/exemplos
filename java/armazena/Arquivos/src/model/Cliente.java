package model;

public class Cliente {
    
    String nome;
    Integer idade;
		
    public Cliente() {
        setNome("nome");
	setIdade(0);
    }
    public Cliente(String nome, Integer idade) {
	setNome(nome);
	setIdade(idade);
    }
    
    public Integer getIdade() {
	return idade;
    }
    public void setIdade(Integer idade) {
	this.idade =  idade;
    }
    
    public String getNome() {
	return nome;
    }
    public void setNome(String nome) {
	this.nome = nome;
    }
}