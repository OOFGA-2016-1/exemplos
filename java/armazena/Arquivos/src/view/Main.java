package view;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import model.Cliente;
public class Main {

    public static void main(String args[]) throws IOException {
        // Cria um array de Clientes.
        ArrayList<Cliente> clientes = new ArrayList<>();
        // Instanciaobjetos de cleinte para fazer testes.
        Cliente cliente1 = new Cliente("Nome_do_Cliente1", 10);
        Cliente cliente2 = new Cliente("Nome_do_Cliente2", 20);
        Cliente cliente3 = new Cliente("Nome_do_Cliente3", 30);
        
        // Adiciona os clientes criados na lista de clientes.
        clientes.add(cliente1);
        clientes.add(cliente2);
        clientes.add(cliente3);        

        // Chama método de gravar no arquivo.
        int tamanhoArray = clientes.size();
        for(int i=0; i < tamanhoArray;i++) {
            // É necessário gravar o "i" pra saber qual o índice do cliente.
            // O "i" funciona como o id do cliente.
            gravaCliente(clientes.get(i), i);            
        }
        
        // Imprime no console o conteúdo do arquivo.
        pegaDoArquivo();
    }
       
    public static void gravaCliente(Cliente cliente, int i) throws IOException {
        // String auxiliar para inserir conteúdo no arquivo. Já é inicializada com o nome do cliente.
        String texto = cliente.getNome();
        // Arquivo a ser criado.
        File file = new File("bancoCliente.txt");
        // Inserção no arquivo.
        BufferedWriter output = null;
        // Tente:
        try {
            // Buffer para gravar a saida.
            output = new BufferedWriter(new FileWriter(file, true));

            // Imprime o ídice do cliente no arquivo.
            output.write(Integer.toString(i));
            output.write(" ");
            
            // Grava o nome do cliente no arquivo.
            output.write(texto);
            
            // Converte o valor inteiro em string pra gravar no arquivo.
            texto = String.valueOf(cliente.getIdade());
            // Pula linhas e grava o valor de idade no arquivo.
            output.write(" ");
            output.write(texto);
            // Quando pular a linha começa um novo cliente, assim é possível organizar os dados.
            output.write("\n");
            
        // Se não conseguir pegue o erro e mostre no console.
        } catch ( IOException e ) {
            // Nada a fazer.
        } finally {
            if ( output != null ) {
                output.close();
            }
        }
    }

    // Método de pegar conteúdo do arquivo.
    public static void pegaDoArquivo() throws IOException {
        // Cria instancia de classe pra ler arquivo.
        FileInputStream banco;
        // Nome do banco.
        banco = new FileInputStream("bancoCliente.txt");
        // O metodo de pegar valor retorna um inteiro, então "aux" guarda esse retorno.
        int aux;
        char letra;
        try {
            // Enquanto tem coisa no arquivo...
                while((aux = banco.read()) != -1) {
                    // pega a letra do arquivo em forma de inteiro e convert pra char.
                    letra = (char)aux;
                    // imprime no console a letra.
                    System.out.print(letra);				
                }			
        } catch (IOException e) {
            // Nada a fazer.
        } finally {
            if (banco != null) {
                banco.close();
            }
        }
    }        
}