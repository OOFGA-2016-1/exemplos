## Orientação a Objetos
### Exemplos usados em monitorias da disciplina de Orientação a Objetos da Universidade de Brasília - Gama (UnB - Gama)

-------------------------------------------------------------------------------

Nesse repositório contém arquivos relacionados aos exercícios e conteúdos de OO, e também com as linguagens de programação utilizadas na disciplina:
* `C++`
* `Java`
* `Ruby` com o framework `Rails` *(Ruby on Rails)*

Cada pasta contém diferentes programas feitos com os alunos para fixar os conteúdos ensinados em sala.

## Exemplos

Vários exemplos estão desorganizados e incompletos de propósito, o objetivo desses exemplos são mostrar como resolver um problema.
É recomendável que o **aluno** corrija o código, aplique os conceitos de OO e termine os exercícios para exercitar suas habilidades de programação.