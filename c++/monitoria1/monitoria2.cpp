#include <fstream>
#include <iostream>
#include <string>
#include <sstream>

using namespace std;

int main(){
  ifstream file("teste2.txt"); 

  if(not file.is_open()){
    cout << "erro na abertura do arquivo" << endl;
    return -1;
  }

  string comentario;
  int altura, largura;

  getline(file, comentario);

  if(comentario[0] != '#'){
    stringstream palavra(comentario);  

    comentario = "sem comentário";

    palavra >> altura;
    palavra >> largura;
  }
  else{
    file >> altura >> largura;
  }

  cout << "comentario: " << comentario << endl;
  cout << altura << " " << largura << endl;
  
  file.close();

  return 0;
}
