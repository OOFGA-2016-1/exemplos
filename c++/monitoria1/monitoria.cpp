#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main() {
  string nome_do_arquivo;

  cout << "entre com o nome do arquivo: ";
  cin >> nome_do_arquivo;

  nome_do_arquivo = "../doc/" + nome_do_arquivo + ".txt";

  ifstream arquivo(nome_do_arquivo.c_str());  

  if(not arquivo.is_open()){
    cout << "erro na abertura do arquivo" << endl;
    return -1;
  }
  
  ofstream arquivo2("arquivo2.txt");  
  string conteudo;

  while(getline(arquivo, conteudo)) {
    cout << conteudo << endl;
    arquivo2 << conteudo << endl;
  }

  arquivo.close();

  return 0;
}
