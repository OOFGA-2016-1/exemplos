#ifndef KELVIN_H
#define KELVIN_H

#include "temperatura.hpp"
class Kelvin : public Temperatura {

	public:
		Kelvin();
		Kelvin(double valor);

		double converter();
		double converter(double valor);
	};

#endif
