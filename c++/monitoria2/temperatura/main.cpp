#include "temperatura.hpp"
#include "kelvin.hpp"

using namespace std;

int main() {

//  Temperatura * temperatura1 = new Temperatura(); //nao pode
  Temperatura * temperatura1 = new Kelvin();
  Kelvin * kelvin = new Kelvin();

  cout << "Informe o valor em Celsius." << endl;

  double valor = 0;
  cin >> valor;

//  cout << "Temperatura " << temperatura1->converter(valor) << endl;
  cout << "Kelvin " << kelvin->converter(valor) << endl;
  return 0;
}
