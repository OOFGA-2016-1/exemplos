#ifndef TEMPERATURA_H
#define TEMPERATURA_H
#include <iostream>
#include <cmath>

class Temperatura {

	private:
		double valor;
	public:
		Temperatura();
		Temperatura(double valor);

		double getValor();
		void setValor(double valor);

		virtual double converter(double valor) = 0;

};

#endif
