#include <iostream>
#include <fstream>
#include <string>
#include <cstdio>

using namespace std;

int main() {
  string nome_do_arquivo;

  cout << "entre com o nome do arquivo: ";
  cin >> nome_do_arquivo;

  nome_do_arquivo = nome_do_arquivo + ".ppm";

  ifstream arquivo(nome_do_arquivo.c_str());  

  if(not arquivo.is_open()){
    cout << "erro na abertura do arquivo" << endl;
    return -1;
  }
  
  ofstream arquivo2("copia.ppm");  
  string conteudo;

  int w, h;
  int max;
  char r, g, b;
  string numero_magico, comentario;

  arquivo >> numero_magico;
  arquivo >> comentario;
  arquivo >> w >> h;
  arquivo >> max;

  arquivo2 << numero_magico << endl;
  arquivo2 << w << " " << h << endl;
  arquivo2 << max << endl;

  printf("%d %d", w, h);
  

  for(int i = 0; i < w; ++i){
    for(int j = 0; j < h; ++j){
      arquivo.get(r);
      arquivo.get(g);
      arquivo.get(b);
      arquivo2.put(r);
      arquivo2.put(g);
      arquivo2.put(b);
      printf("(%d, %d, %d) ", (int) r, (int) g, (int) b);
    }
  }

  arquivo.close();
  arquivo2.close();

  return 0;
}
